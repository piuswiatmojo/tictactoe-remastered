package com.btpn.chip;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Board {
    private final List<Token> tokens;

    public Board(List<Token> tokens) {
        this.tokens = new ArrayList<>(tokens);
    }

    public static Board parse(String gameInput) {
        List<Token> tokens = Token.parse(gameInput);

        return new Board(tokens);
    }

    private void checkDiagonalWinners(List<Token> winnerTokens) {
        if (this.tokens.get(2) == this.tokens.get(4) && this.tokens.get(4) == this.tokens.get(6)) {
            winnerTokens.add(this.tokens.get(6));
        }
        if (this.tokens.get(0) == this.tokens.get(4) && this.tokens.get(4) == this.tokens.get(8)) {
            winnerTokens.add(this.tokens.get(8));
        }
    }

    private void checkColumnWinners(List<Token> winnerTokens) {
        if (this.tokens.get(0) == this.tokens.get(3) && this.tokens.get(3) == this.tokens.get(6)) {
            winnerTokens.add(this.tokens.get(0));
        }
        if (this.tokens.get(1) == this.tokens.get(4) && this.tokens.get(4) == this.tokens.get(7)) {
            winnerTokens.add(this.tokens.get(0));
        }
        if (this.tokens.get(2) == this.tokens.get(5) && this.tokens.get(5) == this.tokens.get(8)) {
            winnerTokens.add(this.tokens.get(0));
        }
    }

    private void checkRowWinners(List<Token> winnerTokens) {
        if (this.tokens.get(0) == this.tokens.get(1) && this.tokens.get(1) == this.tokens.get(2)) {
            winnerTokens.add(this.tokens.get(0));
        }
        if (this.tokens.get(3) == this.tokens.get(4) && this.tokens.get(4) == this.tokens.get(5)) {
            winnerTokens.add(this.tokens.get(3));
        }
        if (this.tokens.get(6) == this.tokens.get(7) && this.tokens.get(7) == this.tokens.get(8)) {
            winnerTokens.add(this.tokens.get(3));
        }
    }

    private String generateWinningMessage(List<Token> winnerTokens) {
        StringBuilder winningMessage = new StringBuilder();
        final int winnerIndex = 0;
        final String winningExpression = " Wins!";
        winningMessage.append(winnerTokens.get(winnerIndex));
        winningMessage.append(winningExpression);
        return winningMessage.toString();
    }

    public String decideWinner() {
        List<Token> winnerTokens = new ArrayList<>();
        this.checkRowWinners(winnerTokens);
        this.checkColumnWinners(winnerTokens);
        this.checkDiagonalWinners(winnerTokens);
        if (winnerTokens.size() > 1) {
            return "Invalid game board";
        }
        if (winnerTokens.size() == 1) {
            return this.generateWinningMessage(winnerTokens);
        }
        if (this.tokens.contains(Token.EMPTY)) {
            return "Game still in progress!";
        }
        return "It's a draw!";
    }
}
