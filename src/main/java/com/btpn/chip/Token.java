package com.btpn.chip;

import java.util.ArrayList;
import java.util.List;

import com.btpn.chip.exception.UnknownTokenException;

public enum Token {
    X, O, EMPTY;

    static final char X_TOKEN_CHARACTER = 'X';
    static final char O_TOKEN_CHARACTER = 'O';
    static final char EMPTY_TOKEN_CHARACTER = '-';

    private static void validateCharacterToken(char token) {
        if (token != X_TOKEN_CHARACTER && token != O_TOKEN_CHARACTER && token != EMPTY_TOKEN_CHARACTER) {
            throw new UnknownTokenException();
        }
    }

    public static List<Token> parse(String stringTokens) {
        List<Token> tokens = new ArrayList<>();
        for (char token : stringTokens.toCharArray()) {
            validateCharacterToken(token);
            if (token == X_TOKEN_CHARACTER) {
                tokens.add(Token.X);
            }
            if (token == O_TOKEN_CHARACTER) {
                tokens.add(Token.O);
            }
            if (token == EMPTY_TOKEN_CHARACTER) {
                tokens.add(Token.EMPTY);
            }
        }
        return tokens;
    }
}
