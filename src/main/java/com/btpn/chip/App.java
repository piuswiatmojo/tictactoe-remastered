package com.btpn.chip;

import java.util.Scanner;

public class App {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        Board gameBoard = Board.parse(scan.nextLine());
        System.out.println(gameBoard.decideWinner());
    }
}
