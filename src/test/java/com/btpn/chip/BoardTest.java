package com.btpn.chip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class BoardTest {
    @ParameterizedTest
    @CsvSource({ "XOXXOOXXO,X Wins!", "OXOOXXOOX,O Wins!", "XOOXOXOXO,O Wins!", "OXOXOXXOX,It's a draw!",
            "XOXX--O--,Game still in progress!", "XXXOOOXXO,Invalid game board", "XXOXXXOOO,Invalid game board",
            "XOXXOXOOX,Invalid game board", "OXXXOOOXO,O Wins!" })
    void decideWinner_shouldReturnXWins_whenGameInputIsXOXXOOXXO(String gameInput, String expectedResult) {
        Board gameBoard = Board.parse(gameInput);

        String winner = gameBoard.decideWinner();

        Assertions.assertEquals(expectedResult, winner);
    }

    @Test
    void parse_shouldCallTokenParserAndReturnBoard_whenInvokedWithStringXOXXOOXXO() {
        String gameInput = "XO-";
        List<Token> expectedBoardTokens = new ArrayList<>(Arrays.asList(Token.X, Token.O, Token.EMPTY));
        Board expectedGameBoard = new Board(expectedBoardTokens);

        Board gameBoard = Board.parse(gameInput);

        Assertions.assertEquals(expectedGameBoard, gameBoard);
    }
}
