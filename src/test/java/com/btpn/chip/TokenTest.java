package com.btpn.chip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.btpn.chip.exception.UnknownTokenException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TokenTest {
    @Test
    void parse_shouldReturnListOfToken_whenInvokedWithStringXAndOAndDash() {
        String stringTokens = "XO-";
        List<Token> expectedTokens = new ArrayList<>(Arrays.asList(Token.X, Token.O, Token.EMPTY));

        List<Token> actualTokens = Token.parse(stringTokens);

        Assertions.assertEquals(expectedTokens, actualTokens);
    }

    @Test
    void parse_shouldThrowUnknownTokenException_whenInvokedWithStringA() {
        String stringTokens = "A";

        Assertions.assertThrows(UnknownTokenException.class, ()->Token.parse(stringTokens));
    }
}
